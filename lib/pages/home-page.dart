import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:go_router_demo/pages/page-one.dart';
import 'package:go_router_demo/pages/page-two.dart';
import 'package:go_router_demo/pages/profile-page.dart';

class HomePage extends StatefulWidget {
  static const String routeName='home';
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(title: Text('Home')),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ElevatedButton(onPressed: (){
              context.goNamed(PageOne.routeName);
            }, child: Text('Page one')),
            ElevatedButton(onPressed: (){
              context.goNamed(PageTwo.routeName,
              extra: 'Hello Flutter');
            }, child: Text('Page two')),
            ElevatedButton(onPressed: (){
              context.goNamed(ProfilePage.routeName,pathParameters: {'id':'1'});
            }, child: Text('Profile')),
          ],
        ),
      ),
    );
  }
}
