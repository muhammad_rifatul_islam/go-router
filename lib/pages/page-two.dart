import 'package:flutter/material.dart';

class PageTwo extends StatefulWidget {
  static const String routeName='two';
  final String greetings;
   PageTwo({super.key,required this.greetings});

  @override
  State<PageTwo> createState() => _PageTwoState();
}

class _PageTwoState extends State<PageTwo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [Text(widget.greetings)],
        ),
      ),
    );
  }
}
