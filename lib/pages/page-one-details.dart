import 'package:flutter/material.dart';

class PageOneDetails extends StatefulWidget {
  static const String routeName='one-details';
  const PageOneDetails({super.key});

  @override
  State<PageOneDetails> createState() => _PageOneDetailsState();
}

class _PageOneDetailsState extends State<PageOneDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [Text('Page One Details')],
        ),
      ),
    );
  }
}
