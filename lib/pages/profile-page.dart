import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:go_router_demo/pages/page-one-details.dart';

class ProfilePage extends StatefulWidget {
  static const String routeName='profile';
  final int id;
   ProfilePage({super.key,required this.id});

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [Text('Profile ${widget.id}')

            ],
        ),
      ),
    );
  }
}
