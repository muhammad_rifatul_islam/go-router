import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:go_router_demo/pages/page-one-details.dart';

class PageOne extends StatefulWidget {
  static const String routeName='one';
  const PageOne({super.key});

  @override
  State<PageOne> createState() => _PageOneState();
}

class _PageOneState extends State<PageOne> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [Text('Page One'),
          ElevatedButton(onPressed: (){
           context.goNamed(PageOneDetails.routeName);

          }, child: Text('Page one  Details here '))],
        ),
      ),
    );
  }
}
