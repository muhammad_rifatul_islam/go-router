

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:go_router_demo/pages/home-page.dart';
import 'package:go_router_demo/pages/page-one-details.dart';
import 'package:go_router_demo/pages/page-one.dart';
import 'package:go_router_demo/pages/page-two.dart';
import 'package:go_router_demo/pages/profile-page.dart';

void main() {
  runApp( MyApp());
}

class MyApp extends StatelessWidget {
   MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      title: 'Go-Router',
      routerConfig: _routes,
      theme: ThemeData(


        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),

    );

  }
  final _routes=GoRouter(
    initialLocation: '/',
    debugLogDiagnostics: true,
    routes: [
      GoRoute(
        name: HomePage.routeName,
        path: '/',
        builder: (context,state) => HomePage(),
        routes:
          [
            GoRoute(
                name: ProfilePage.routeName,
                path: 'profile/:id',
                builder: (context,state){
                  final id=state.pathParameters['id'];
                  return ProfilePage(id:  int.parse(id!),);
                }
            ),
          ]
             ),
      GoRoute(
         name: PageOne.routeName,
          path: '/one',
          builder: (context,state) => PageOne(),
          routes: [
            GoRoute(
              name: PageOneDetails.routeName,
                path: 'one-details',
            builder: (context,state)=>PageOneDetails())
          ]
      ),
      GoRoute(
         name: PageTwo.routeName,
          path: '/two',
          builder: (context,state){

           final msg=state.extra as String;
           return PageTwo(greetings: msg);
          }
      ),
    ]
  );
}
